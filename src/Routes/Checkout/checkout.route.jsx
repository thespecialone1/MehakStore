
import CheckoutTable from "../../Components/checkout-table/checkout-table.component";
import "./checkout.style.scss";

const Checkout = () => {
    return (
        <div className="checkout body-max-width">
            <h1>Checkout</h1>
            <CheckoutTable />


        </div>
    )

}

export default Checkout;